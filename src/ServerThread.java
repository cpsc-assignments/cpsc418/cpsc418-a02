/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   ServerThread.java
 */

import crypto.Decryption;
import utils.CryptoUtils;
import utils.Log;
import utils.Utils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.net.*;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Thread to deal with clients who connect to Server.  Put what you want the
 * thread to do in it's run() method.
 */
public class ServerThread extends Thread {
    private Socket clientSocket; //The clientSocket it communicates with the client on.
    private Server server;       //Reference to Server object for message passing.
    private int clientId;        //The client's id number.
    private BufferedReader in;
    private DataInputStream dataIn;
    private PrintWriter out;
    private Decryption decryption;

    /**
     * Constructor, does the usual stuff.
     *
     * @param clientSocket  Communication Socket.
     * @param port  Reference to server thread.
     * @param id ID Number.
     */
    public ServerThread(Socket clientSocket, Server port, int id) {
        this.server = port;
        this.clientSocket = clientSocket;
        this.clientId = id;
        this.decryption = Decryption.getInstance().setSeed(server.getServerSeed());

        try {
            this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.dataIn = new DataInputStream(clientSocket.getInputStream());
            this.out = new PrintWriter(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is what the thread does as it executes.  Listens on the clientSocket
     * for incoming data and then echos it to the screen.  A client can also
     * ask to be disconnected with "exit" or to shutdown the server with "die".
     */
    @Override
    public void run() {
        try {
            byte[] receivedSrcHash = receiveData();
            byte[] receivedSrc = receiveData();
            byte[] receivedDestHash = receiveData();
            byte[] receivedDest = receiveData();

            byte[] src = decryption.decrypt(receivedSrc).getData();
            String dest = decryption.decrypt(receivedDest).getString();

            boolean srcIsValid = CryptoUtils.validate(receivedSrcHash, src, server.getServerSeed());
            boolean destIsValid = CryptoUtils.validate(receivedDestHash, dest.getBytes(), server.getServerSeed());

            Log.debug("valid src received: %s", srcIsValid);
            Log.debug("valid dest received: %s", destIsValid);

            if (!srcIsValid && !destIsValid) {
                out.println(false);
                out.flush();
            } else {
                boolean success = Utils.writeDataToPath(src, dest);

                Log.debug("file written: %s", success);

                out.println(success);
                out.flush();
            }

        } catch (UnknownHostException e) {
            System.out.println("Unknown host error.");
        } catch (IOException e) {
            if (server.getFlag()) {
                System.out.println("shutting down.");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for id number.
     *
     * @return ID Number
     */
    public int getID() {
        return clientId;
    }

    /**
     * Getter for the clientSocket, this way the server thread can
     * access the clientSocket and close it, causing the thread to
     * stop blocking on IO operations and see that the server's
     * shutdown flag is true and terminate.
     *
     * @return The Socket.
     */
    public Socket getClientSocket() {
        return clientSocket;
    }

    private byte[] receiveData() throws IOException {
        byte[] in;
        int receivedSrcLen = dataIn.readInt();

        if (receivedSrcLen > 0) {
            in = new byte[receivedSrcLen];
            dataIn.readFully(in, 0, in.length);
            return in;
        } else {
            return null;
        }
    }

    @Override
    public void interrupt() {
        super.interrupt();
        try {
            this.getClientSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
