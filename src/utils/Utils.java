/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Utils.java
 */

package utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;

public class Utils {
    private static final String HEXES = "0123456789ABCDEF";

    public static byte[] readBytes(FileInputStream in) throws IOException {
        byte[] msg = new byte[in.available()];
        in.read(msg);
        return msg;
    }

    public static byte[] readBytes(InputStream in) throws IOException {
        byte[] msg = new byte[in.available()];
        in.read(msg);
        return msg;
    }

    public static String getHexString(byte[] raw) {
        if (raw == null) {
            return null;
        }

        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                .append(HEXES.charAt((b & 0x0F)));
        }

        return hex.toString();
    }

    public static byte[] mergeArrays(byte[] a, byte[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        byte[] c = (byte[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public static boolean writeDataToPath(byte[] src, String dest) {
        boolean success = false;

        try {
            FileOutputStream fileOut = new FileOutputStream(dest);
            fileOut.write(src);
            fileOut.close();
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return success;
    }
}
