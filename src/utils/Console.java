/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Console.java
 */

package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    private BufferedReader in;
    private static Console instance;

    private Console() {
        in = new BufferedReader(new InputStreamReader(System.in));
    }

    private Console(BufferedReader in) {
        this.in = in;
    }

    public static synchronized Console newInstance() {
        if (instance == null) {
            instance = new Console();
        }

        return instance;
    }

    public static synchronized Console newInstance(BufferedReader in) {
        if (instance == null) {
            instance = new Console();
        }

        return instance;
    }

    public String prompt(String prompt) {
        String res = null;
        try {
            System.out.print(prompt);
            res = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }
}
