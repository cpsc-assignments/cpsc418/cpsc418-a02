/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Constants.java
 */

package utils;

public class Constants {
    public static final int IV_SIZE = 16;
    public static final int SHA256_HASH_LENGTH = 32;
}
