/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Log.java
 */

package utils;

public class Log {
    public static boolean debug = false;

    public static void debug(Object s, Object... args) {
        if (debug)
            out("DEBUG: " + s, args);
    }

    public static void out(Object s, Object... args) {
        System.out.printf(s + "\n", args);
    }
}
