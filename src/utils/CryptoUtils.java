/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   CryptoUtils.java
 */

package utils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.util.Arrays;

public class CryptoUtils {
    public static byte[] SHA256Hash(byte[] inputData) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-256");
        return sha1.digest(inputData);
    }

    public static SecureRandom getSecureRandom() throws NoSuchAlgorithmException {
        return SecureRandom.getInstance("SHA1PRNG");
    }

    public static SecretKey getPRNGKey(String seed) throws NoSuchAlgorithmException {
        SecureRandom secureRandom = getSecureRandom();
        secureRandom.setSeed(seed.getBytes());

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128, secureRandom);
        return keyGenerator.generateKey();
    }

    public static Cipher getAESCipher(int mode, IvParameterSpec ivParameterSpec, String seed) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, getPRNGKey(seed), ivParameterSpec);
        return cipher;
    }

    public static byte[] HMACSHA1(byte[] data, String seed) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] res;
        SecretKey secretKey = getPRNGKey(seed);

        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);

        res = mac.doFinal(data);

        return res;
    }

    public static byte[] HMACSHA1(File f, String seed) throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return HMACSHA1(Utils.readBytes(new FileInputStream(f)), seed);
    }

    public static boolean validate(byte[] hash, byte[] data, String seed) throws InvalidKeyException, NoSuchAlgorithmException {
        return Arrays.equals(hash, HMACSHA1(data, seed));
    }
}
