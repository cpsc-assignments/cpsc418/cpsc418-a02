/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Decryption.java
 */

package crypto;

import utils.*;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Decryption {
    private static Decryption instance;

    private byte[] hash;
    private byte[] decryptedData;
    private String seed;

    public static Decryption getInstance() {
        if (instance == null) {
            instance = new Decryption();
        }

        return instance;
    }

    public Decryption setSeed(String seed) {
        this.seed = seed;
        return this;
    }

    public Decryption decrypt(String filename) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException {
        byte[] inputBytes = Utils.readBytes(new FileInputStream(filename));
        decrypt(inputBytes);
        return this;
    }

    public Decryption decrypt(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException {
        byte[] iv = new byte[Constants.IV_SIZE];
        byte[] hash = new byte[Constants.SHA256_HASH_LENGTH];

        Cipher cipher = CryptoUtils.getAESCipher(Cipher.DECRYPT_MODE, new IvParameterSpec(iv), this.seed);
        byte[] decrypted = cipher.doFinal(data);

        int fileSize = decrypted.length - iv.length - hash.length;
        byte[] file = new byte[fileSize];

        System.arraycopy(data, 0, iv, 0, iv.length);
        System.arraycopy(decrypted, iv.length, hash, 0, hash.length);
        System.arraycopy(decrypted, hash.length + iv.length, file, 0, file.length);

        setHash(hash);
        setDecryptedData(file);

        return this;
    }

    public boolean writeResult(String to) throws NoSuchAlgorithmException {
        boolean success = false;

        try {
            FileOutputStream fileOut = new FileOutputStream(to);
            if (isValidHash()) {
                fileOut.write(this.decryptedData);
                fileOut.close();
                success = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return success;
    }

    public String getString() throws NoSuchAlgorithmException {
        return isValidHash() ? new String(this.decryptedData) : null;
    }

    public byte[] getData() throws NoSuchAlgorithmException {
        return isValidHash() ? this.decryptedData : null;
    }

    private boolean isValidHash() throws NoSuchAlgorithmException {
        return Arrays.equals(this.hash, CryptoUtils.SHA256Hash(this.decryptedData));
    }

    private void setDecryptedData(byte[] decryptedData) {
        this.decryptedData = decryptedData;
    }

    private void setHash(byte[] hash) {
        this.hash = hash;
    }
}
