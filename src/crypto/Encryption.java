/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Encryption.java
 */

package crypto;

import utils.*;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Encryption {
    private static Encryption instance;

    private byte[] encryptedData;
    private String seed;

    public static Encryption getInstance() {
        if (instance == null) {
            instance = new Encryption();
        }

        return instance;
    }

    public Encryption setSeed(String seed) {
        this.seed = seed;
        return this;
    }

    public static Cipher getCipher(String seed) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException {
        byte[] iv = new byte[Constants.IV_SIZE];
        CryptoUtils.getSecureRandom().nextBytes(iv);
        return CryptoUtils.getAESCipher(Cipher.ENCRYPT_MODE, new IvParameterSpec(iv), seed);
    }

    public Encryption encrypt(String filename) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException {
        encrypt(new File(filename));
        return this;
    }

    public Encryption encrypt(File file) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException {
        byte[] inputBytes = Utils.readBytes(new FileInputStream(file));
        encrypt(inputBytes);
        return this;
    }

    public Encryption encrypt(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException {
        byte[] sha256Hash = CryptoUtils.SHA256Hash(data);
        byte[] message = Utils.mergeArrays(sha256Hash, data);

        byte[] iv = new byte[Constants.IV_SIZE];
        CryptoUtils.getSecureRandom().nextBytes(iv);

        Cipher cipher = CryptoUtils.getAESCipher(Cipher.ENCRYPT_MODE, new IvParameterSpec(iv), seed);
        byte[] encryptedMessage = cipher.doFinal(message);
        byte[] encrypted = Utils.mergeArrays(iv, encryptedMessage);

        setEncryptedData(encrypted);

        return this;
    }

    public boolean writeResult(String to)  {
        boolean success = false;

        try {
            FileOutputStream fileOut = new FileOutputStream(to);
            fileOut.write(this.encryptedData);
            fileOut.close();

            success = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return success;
    }

    public byte[] getResult()  {
        return this.encryptedData;
    }

    private void setEncryptedData(byte[] data) {
        this.encryptedData = data;
    }
}
