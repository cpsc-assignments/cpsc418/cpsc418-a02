/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Client.java
 */

import crypto.Encryption;
import utils.Console;
import utils.CryptoUtils;
import utils.Log;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Client program.  Connects to the server and sends text accross.
 */

public class Client {
    private final int port;
    private final String ip;
    private Socket socket;

    private String clientSeed;
    private String destPath;
    private String sourcePath;

    private DataOutputStream out;
    private BufferedReader in;
    private Console console = Console.newInstance();
    private Encryption encryption;

    /**
     * Constructor, in this case does everything.
     *
     * @param ip   The hostname to connect to.
     * @param port The port to connect to.
     */
    public Client(String ip, int port) {
        this.ip = ip;
        this.port = port;
        encryption = Encryption.getInstance().setSeed(getClientSeed());

        try {
            this.socket = new Socket(InetAddress.getByName(ip), port);

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            DataOutputStream dataOut = new DataOutputStream(socket.getOutputStream());

            setOutputStream(dataOut);
            setInputStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Establishes connection to server, whose address and port was passed to constructor.
     * Upon connection, the client is prompted for:
     *   - Shared key
     *   - Path to source file, that will be sent to server
     *   - File destination
     * If shared key, entered by client is correct, the source and destination will be delivered correctly,
     *  otherwise, the server will respond with failure and client will abort with failure message.
     *
     * If file was decrypted and written to specified destination, the client will abort with message notifying user
     *  about successful transfer.
     */
    public void establishConnection() {
        try {
            Log.out("Connected to %s:%d", socket.getInetAddress().getHostAddress(), port);

            setClientSeed(console.prompt("Enter Client key: "));


            setSourcePath(console.prompt("src: "));
            setDestPath(console.prompt("dest: "));

            File src = new File(getSourcePath());

            byte[] srcHash = CryptoUtils.HMACSHA1(src, getClientSeed());
            byte[] encryptedSrc = encryption.encrypt(src).getResult();

            byte[] destHash = CryptoUtils.HMACSHA1(src, getClientSeed());
            byte[] encryptedDest = encryption.encrypt(getDestPath().getBytes()).getResult();

            sendDataToServer(srcHash);
            sendDataToServer(encryptedSrc);

            sendDataToServer(destHash);
            sendDataToServer(encryptedDest);

            boolean success = Boolean.parseBoolean(in.readLine());

            if (success) {
                Log.out("Files were delivered successfully");
                socket.close();
            } else {
                Log.out("Files were not delivered successfully");
                socket.close();
            }

        } catch (IOException e) {
            Log.out("Could not read from input.");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException |
                 InvalidKeyException | InvalidAlgorithmParameterException |
                 IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
    }

    private void sendDataToServer(byte[] data) throws IOException {
        out.writeInt(data.length);
        out.write(data);
    }

    private String getClientSeed() {
        return clientSeed;
    }

    private void setClientSeed(String seed) {
        this.clientSeed = seed;
    }

    private void setDestPath(String destPath) {
        this.destPath = destPath;
    }

    private String getDestPath() {
        return destPath;
    }

    private void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    private String getSourcePath() {
        return sourcePath;
    }

    private void setOutputStream(DataOutputStream dataOut) {
        this.out = dataOut;
    }

    private void setInputStream(BufferedReader in) {
        this.in = in;
    }

    /**
     * Main method, starts the client.
     *
     * @param args args[0] needs to be a hostname, args[1] a port number.
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java Client hostname port#");
            System.out.println("hostname is a string identifying your server");
            System.out.println("port is a positive integer identifying the port to connect to the server");
            return;
        }

        try {
            String address = args[0];
            int port = Integer.parseInt(args[1]);

            Client c = new Client(address, port);
            c.establishConnection();
        } catch (NumberFormatException e) {
            System.out.println("Usage: java Client hostname port#");
            System.out.println("Second argument was not a port number");
        }
    }
}
