# Author: Artem Golovin
# UID:    30018900
# Email:  artem.golovin@ucalgary.ca

SRC_DIR = ./src/
JAVA_SRC = $(shell find $(SRC_DIR) -name "*.java") 
CLASS_DIR = ./out

build:
	@mkdir -p $(CLASS_DIR)
	@javac -d $(CLASS_DIR) -cp src $(JAVA_SRC)

build_verbose:
	@mkdir -p $(CLASS_DIR)
	@javac -d $(CLASS_DIR) -cp src $(JAVA_SRC) -verbose

clear:
	rm -f ./**/*.class
